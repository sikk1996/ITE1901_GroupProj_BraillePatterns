import java.util.Scanner;

public class testBrailleTranslator {

    public static void main(String[] args) {
        BrailleTranslator bt = new BrailleTranslator();
        Parser parser = new Parser();
        Scanner input = new Scanner(System.in);
        boolean loop = true;

        while (loop) {
            System.out.println("1: Alfa to Braille");
            System.out.println("2: Translate file: Alfa to Braille");
            System.out.println("3: Braille to Alfa");
            System.out.println("4: Translate file: Braille to Alfa");
            System.out.println("5: Exit");

            switch (input.nextInt()) {
                case 1:
                    input.nextLine();
                    System.out.print("input: ");
                    bt = new BrailleTranslator(input.nextLine(), Translator.ALFA_TO_BRAILLE_MODE);
                    bt.translate();
                    System.out.println(bt.listToString(bt.getList()));
                    break;
                case 2:
                    input.nextLine();
                    System.out.println("File path? ");
                    bt.setMode(Translator.ALFA_TO_BRAILLE_MODE);
                    bt.translateFile(input.nextLine(), "braille.txt", parser);
                    break;
                case 3:
                    input.nextLine();
                    System.out.print("input: ");
                    bt = new BrailleTranslator(input.nextLine(), Translator.BRAILLE_TO_ALFA_MODE);
                    bt.translate();
                    System.out.println(bt.listToString(bt.getList()));
                    break;
                case 4:
                    input.nextLine();
                    System.out.println("File path?");
                    bt.setMode(Translator.BRAILLE_TO_ALFA_MODE);
                    bt.translateFile(input.nextLine(), "alfa.txt", parser);
                    break;
                case 5:
                    loop = false;
                    break;
                default:
                    System.out.println("Pick option 1 to 5");
                    break;
            }
        }
    }
}