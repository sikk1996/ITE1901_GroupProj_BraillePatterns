import org.apache.commons.collections4.bidimap.DualHashBidiMap;

public interface Dictionary {
    DualHashBidiMap<Character, Character> letterCharacterMap = new DualHashBidiMap<>() {{
        put('a', (char) 10241);
        put('b', (char) 10243);
        put('c', (char) 10249);
        put('d', (char) 10265);
        put('e', (char) 10257);
        put('f', (char) 10251);
        put('g', (char) 10267);
        put('h', (char) 10259);
        put('i', (char) 10250);
        put('j', (char) 10266);
        put('k', (char) 10245);
        put('l', (char) 10247);
        put('m', (char) 10253);
        put('n', (char) 10269);
        put('o', (char) 10261);
        put('p', (char) 10255);
        put('q', (char) 10271);
        put('r', (char) 10263);
        put('s', (char) 10254);
        put('t', (char) 10270);
        put('u', (char) 10277);
        put('v', (char) 10279);
        put('w', (char) 10298);
        put('x', (char) 10285);
        put('y', (char) 10301);
        put('z', (char) 10293);
    }};

    DualHashBidiMap<Character, Character> cipherCharacterMap = new DualHashBidiMap<>() {{
        put('0', (char) 10266);
        put('1', (char) 10241);
        put('2', (char) 10243);
        put('3', (char) 10249);
        put('4', (char) 10265);
        put('5', (char) 10257);
        put('6', (char) 10251);
        put('7', (char) 10267);
        put('8', (char) 10259);
        put('9', (char) 10250);
    }};

    DualHashBidiMap<Character, Character> punctuationCharacterMap = new DualHashBidiMap<>() {{
        put('.', (char) 10290); //2832
        put(',', (char) 10242); //2802
        put(':', (char) 10258); //2812
        put(';', (char) 10246); //2806
        put('!', (char) 10262); //2816
        put('?', (char) 10278); //2826
        put('(', (char) 10294); //2836
        put('/', (char) 10252); //280C
        put('\\', (char) 10273); //2821
        put('#', (char) 10281); //2829
        put('\'', (char) 10300); //283C
        put('-', (char) 10276); //2824
    }};

    DualHashBidiMap<String, Character> contractionMap = new DualHashBidiMap<>() {{
        put("but", (char) 10243);
        put("can", (char) 10249);
        put("do", (char) 10265);
        put("every", (char) 10257);
        put("from", (char) 10251);
        put("go", (char) 10267);
        put("have", (char) 10259);
        put("just", (char) 10266);
        put("knowledge", (char) 10280);
        put("like", (char) 10296);
        put("more", (char) 10253);
        put("not", (char) 10269);
        put("people", (char) 10255);
        put("quite", (char) 10271);
        put("rather", (char) 10263);
        put("so", (char) 10254);
        put("that", (char) 10270);
        put("us", (char) 10277);
        put("very", (char) 10279);
        put("it", (char) 10285);
        put("you", (char) 10301);
        put("as", (char) 10293);
        put("and", (char) 10287);
        put("for", (char) 10303);
        put("of", (char) 10295);
        put("the", (char) 10286);
        put("with", (char) 10302);
        put("will", (char) 10298);
        put("his", (char) 10278);
        put("in", (char) 10260);
        put("was", (char) 10292);
        put("to", (char) 10262);
    }};
}