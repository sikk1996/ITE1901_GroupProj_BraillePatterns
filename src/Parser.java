import java.io.*;
import java.util.Scanner;

public class Parser {
    private File file;
    public String lastIn = "";
    public String lastOut = "";

    public String readFile(String path) throws IOException {
        file = new File(path);
        StringBuilder stringBuilder = new StringBuilder(100);
        if (file.exists() && file.canRead() && file.length() != 0) {
            Scanner input = new Scanner(new File(file.getAbsolutePath()));

            while (input.hasNextLine()) {
                String nextLine = input.nextLine().trim() + " ";
                stringBuilder.append(nextLine);
            }
            input.close();
            stringBuilder = new StringBuilder(stringBuilder.toString().replaceAll(" +", " "));
            stringBuilder.trimToSize();
        } else {
            throw new IOException();
        }
        lastIn = stringBuilder.toString().trim();
        return lastIn;
    }

    public boolean writeFile(String path, String output) {
        file = new File(path);
        lastOut = output;

        try {
            PrintWriter printWriter = new PrintWriter(file.getAbsolutePath());
            printWriter.write(output);
            printWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}