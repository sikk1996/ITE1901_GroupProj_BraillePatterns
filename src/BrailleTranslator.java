import java.io.IOException;
import java.util.*;

public class BrailleTranslator implements Translator, Dictionary {
    private LinkedList<String> list;
    private int mode;

    public BrailleTranslator() {
        list = stringToList("This is a default list");
        mode = ALFA_TO_BRAILLE_MODE;
    }

    public BrailleTranslator(String text, int mode) {
        this.list = stringToList(text);
        this.mode = mode;
    }

    public static String translate(String input, int mode) {
        BrailleTranslator bt = new BrailleTranslator(input, mode);
        bt.translate();
        return bt.listToString(bt.getList());
    }

    public void translateFile(String inputPath, String outputName, Parser parser) {
        try {
            list = stringToList(parser.readFile(inputPath));
            translate();
            parser.writeFile(outputName, listToString(list));
            System.out.println("Translation saved in " + outputName);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("File error");
        }
    }

    @Override
    public void translate() {
        if (mode == BRAILLE_TO_ALFA_MODE) {
            numberExtractor();
            brailleToAlfaContractions();
            brailleToAlfaBuildTranslation();
            capsHandler();
        } else if (mode == ALFA_TO_BRAILLE_MODE) {
            numberExtractor();
            capsHandler();
            buildTranslation();
        }
    }

    @Override
    public void numberExtractor() {
        ListIterator<String> iterator = list.listIterator();
        StringBuilder newWord;
        char[] chars;
        if (mode == ALFA_TO_BRAILLE_MODE) {
            while (iterator.hasNext()) {
                boolean firstNumb = true;
                newWord = new StringBuilder();
                chars = iterator.next().toCharArray();
                for (char c : chars) {
                    if (Character.isDigit(c)) {
                        if (firstNumb) {
                            newWord.append('⠼');
                            firstNumb = false;
                        }
                        newWord.append(cipherCharacterMap.get(c));
                    } else {
                        firstNumb = true;
                        newWord.append(c);
                    }
                }
                iterator.set(newWord.toString());
            }
        } else {
            while (iterator.hasNext()) {
                newWord = new StringBuilder();
                String word = iterator.next();
                if (word.contains("⠼")) {
                    int start = word.indexOf("⠼") + 1;
                    chars = word.toCharArray();
                    newWord.append(word.substring(0, start - 1));
                    for (; start < word.length(); start++) {
                            newWord.append(cipherCharacterMap.getKey(chars[start]));
                    }
                } else {
                    newWord = new StringBuilder(word);
                }
                iterator.set(newWord.toString());
            }
        }
    }

    @Override
    public void capsHandler() {
        ListIterator<String> iterator = list.listIterator();
        StringBuilder newWord;
        if (mode == ALFA_TO_BRAILLE_MODE) {
            while (iterator.hasNext()) {
                newWord = new StringBuilder();
                char[] chars = iterator.next().toCharArray();
                for (char c : chars) {
                    if (Character.isUpperCase(c)) {
                        newWord.append('⠠');
                    }
                    newWord.append(Character.toLowerCase(c));
                }
                iterator.set(newWord.toString());
            }
        } else {
            boolean uppercase = false;
            while (iterator.hasNext()) {
                newWord = new StringBuilder();
                String buffer = iterator.next();
                if (buffer.contains("⠠")) {
                    char[] chars = buffer.toCharArray();
                    for (char c : chars) {
                        if (c == '⠠') {
                            uppercase = true;
                            continue;
                        }
                        if (uppercase) {
                            newWord.append((Character.toUpperCase(c)));
                            uppercase = false;
                        } else {
                            newWord.append(c);
                        }
                    }
                } else {
                    newWord = new StringBuilder(buffer);
                }
                iterator.set(newWord.toString());
            }
        }
    }

    @Override
    public void buildTranslation() {
        ListIterator<String> iterator = list.listIterator();
        StringBuilder newWord;
        String buffer;
        char[] chars;
        while (iterator.hasNext()) {
            newWord = new StringBuilder();
            buffer = iterator.next();

            // Contractions
            for (Map.Entry<String, Character> entry : contractionMap.entrySet()) {
                if (buffer.contains(entry.getKey())) {
                    buffer = buffer.replaceAll("^((\\W?)" + entry.getKey() + "(\\W?))$", "$2" + entry.getValue() + "$3");
                }
            }
            chars = buffer.toCharArray();

            // Translate remaining
            for (char c : chars) {
                if (letterCharacterMap.containsKey(c)) {
                    newWord.append(letterCharacterMap.get(c));
                } else if (punctuationCharacterMap.containsKey(c)) {
                    newWord.append(punctuationCharacterMap.get(c));
                } else if (c=='–') {
                    newWord.append(punctuationCharacterMap.get('-'));
                    newWord.append(punctuationCharacterMap.get('-'));
                } else if (c==')') {
                    newWord.append(punctuationCharacterMap.get('('));
                } else {
                    newWord.append(c);
                }
            }
            iterator.set(newWord.toString());
        }
    }

    @Override
    public void brailleToAlfaContractions() {
        ListIterator<String> iterator = list.listIterator();
        String buffer;
        while (iterator.hasNext()) {
            buffer = iterator.next();
            for (Map.Entry<String, Character> entry : contractionMap.entrySet()) {
                if (buffer.contains(entry.getValue() + "")) {
                    buffer = buffer.replaceAll("^([\\u2832\\u2802\\u2812\\u2806\\u2817\\u2826\\u2836\\u280C\\u2821\\u2829\\u283C\\u2824\\u2820]?)" + entry.getValue() + "([\\u2832\\u2802\\u2812\\u2806\\u2817\\u2826\\u2836\\u280C\\u2821\\u2829\\u283C\\u2824\\u2820]?)$", "$1" + entry.getKey() + "$2");
                }
            }
            iterator.set(buffer);
        }
    }

    @Override
    public void brailleToAlfaBuildTranslation() {
        ListIterator<String> iterator = list.listIterator();
        String buffer;
        StringBuilder newWord;
        char[] chars;

        while (iterator.hasNext()) {
            buffer = iterator.next();
            chars = buffer.toCharArray();
            newWord = new StringBuilder();

            // Translate remaining
            for (int i = 0; i < chars.length; i++) {
                if (letterCharacterMap.containsValue(chars[i])) {
                    newWord.append(letterCharacterMap.getKey(chars[i]));
                } else if (punctuationCharacterMap.containsValue(chars[i])) {
                    if (chars[i] == '⠠') {
                        if (!(i == chars.length - 1 || chars[i + 1] == ' ')) {
                            newWord.append(chars[i]);
                        } else {
                            newWord.append(punctuationCharacterMap.getKey(chars[i]));
                        }
                    } else if (chars[i]=='⠶'){
                        if (!(i == chars.length - 1 || chars[i + 1] == ' ')){
                            newWord.append("(");
                        } else {
                            newWord.append(")");
                        }
                    }else {
                        newWord.append(punctuationCharacterMap.getKey(chars[i]));
                    }
                } else {
                    newWord.append(chars[i]);
                }
            }
            if (newWord.toString().contains("--")){
                newWord = new StringBuilder(newWord.toString().replaceAll("--","–"));
            }
            iterator.set(newWord.toString());
        }
    }

    public LinkedList<String> getList() {
        return list;
    }

    public String getText() {
        return listToString(list);
    }

    public void setMode(int mode) {
        this.mode = mode;
    }
}