import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

public interface Translator {
    int ALFA_TO_BRAILLE_MODE = 0;
    int BRAILLE_TO_ALFA_MODE = 1;

    void translate();

    void translateFile(String inputPath, String outputName, Parser parser);

    default LinkedList<String> stringToList(String in) {
        in = in.replaceAll("\n", " ");
        in = in.replaceAll("  ", " ");
        LinkedList<String> out = new LinkedList<>(Arrays.asList(in.split(" ")));
        return out;
    }

    default String listToString(LinkedList<String> in) {
        StringBuilder out = new StringBuilder();
        for (String word : in) {
            out.append(word + " ");
        }
        out.deleteCharAt(out.length() - 1);
        return out.toString();
    }

    void numberExtractor();

    void capsHandler();

    void buildTranslation();

    void brailleToAlfaContractions();

    void brailleToAlfaBuildTranslation();
}