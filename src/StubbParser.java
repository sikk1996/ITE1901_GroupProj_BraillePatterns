public class StubbParser extends Parser {

    @Override
    public String readFile(String path) {
        lastIn = (path == "alfa.txt") ? "Text to translate to braille! what? Nice, dude."
                : "⠠⠞⠑⠭⠞ ⠖ ⠞⠗⠁⠝⠎⠇⠁⠞⠑ ⠖ ⠃⠗⠁⠊⠇⠇⠑⠮ ⠺⠓⠁⠞⠹ ⠠⠝⠊⠉⠑⠠ ⠙⠥⠙⠑⠨";
        return lastIn;
    }

    public boolean writeFile(String path, String output) {
        lastOut = output;
        return true;
    }
}