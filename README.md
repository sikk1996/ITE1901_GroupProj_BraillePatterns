**Prosjektarbeid i ITE1901 Programmering 2**
<br>
Gruppemedlemmer: Lene Tangstad og Peter Schjem
<br>

**Tolkning av oppgaven:**
<br>
Vi har fått i oppgave å lage et oversettelsesprogram fra alfabetisk til Braille og motsatt.
Braille er et skriftspråk, ofte kalt blindeskrift og brukes hovedsaklig av blinde og svaksynte.
Det som er utfordrende er at Braille ikke kan oversettes bokstav for bokstav - da blir det feil.
Dette er blant annet på grunn av at det finnes sammentrekninger av ord og bokstaver, fra flere symboler til et.
Det er også slik at det ikke skilles på store og små bokstaver i Braille, men det brukes noe som kalles en escape-character,
som er et tegn som brukes i nærheten av andre tegn for å gi en indikasajon på hvordan det skal leses.
Disse escape-characterene har ikke noe tilsvarende tegn i norsk og engelsk skriftspråk.

Vi har tolket oppgaven til å oversette til og fra English Braille grad 2, som er en grad som inkluderer sammentrekninger.
Vi fikk vedlagt en tabell med de ulike characters som skulle inkluderes i oppgaven, og har ikke gått utover disse.
Da det ikke ble gitt noen spesifikk veiledning til å når og hvordan sammentrekninger skal foregå, har vi antatt at det var åpent.
Derfor har vi valgt å oversette når hele ordet tilsvarer det som kan sammentrekkes, og ikke når det er midt i ordet.
<br>

**Dette har vi gjort:**
<br>
1: _Tester_
<br>

I dette prosjektet skal vi vise hva vi har lært i forbindelse med TDD (Test-Driven Development). 
Derfor har tester vært fokus nr. 1. i dette prosjektet. Vi skiller hovedsaklig inn i tre ulike typer testing: 
enhetstesting (tester en og en enhet for seg selv), integrasjonstesting (tester enhetene i sammenheng med hverandre)
og testing av brukergrensesnitt (hvordan sluttproduktet blir for kunden/brukeren).

Vi har tatt utgangspunkt i red-green-refactoring, som er elementært i TDD. 
Det går ut på å først lage testen og oppgi hva som er forventet output i metodene som skal lages, første gang den kjøres blir den rød.
Deretter skriver du koden slik at du får du testene til å bli godkjente, altså grønne. Til slutt refaktorerer du
koden slik at den dekker de nødvendige funksjonene, og blir enkel og ryddig.

Triangulering er en fremgangsmåte der man sender ulike verdier/variabler inn i metodene og sørger for at alt kjører grønt.
Med flere ulike verdier, finner man fortere feil og mulige forbedringer som skal gjøres. Spesielt hvis kun deler av testene ikke funker,
så vil man kunne fortere se hvor koden har mangler.

En annen ting som TDD fører med seg er det at når man skiver tester, skriver man ofte koden så enkel som mulig kun for å få testen til 
å bli godkjent. Dette fører med seg både enklere og "renere" kode. "Keep it simple, stupid", "You aren't gonna need it"
og "Fake it 'til you make it" er prinsipper som settes i sammenheng med TDD. Den første går ut på nettopp
dette om at koden skal være så enkel og oversiktig som mulig, den neste går ut på at man ikke skal trenge å lage metoder
som er unødvendige for sluttresultatet. Dette får man ved å ta et steg av gangen og bare skrive kode når man trenger den -
ikke skriv kode fordi man tror man trenger den senere. Da skal man i utgangspunktet spare både tid og penger.
Det siste prinsippet handler om den røde til grønne delen av r-g-r, altså starten i red-green-refactoring.

Om man vil at en metode skal returnere et tall, kan man enten "stubbe" (hardkoding for ønsket output)
den inntil videre eller gjøre noe for å få testen grønn, selv om den er enklere enn den må være i det endelige produktet.
Og derifra kan man jobbe videre med å gjøre koden fullverdig.

Vi burde kanskje vise at vi har lært å bruke prinsippet "keep it simple, stupid", 
derfor skal vi være mer to-the-point videre i dette dokumentet :)

2: _Brukt interface som Dictionary vha DualHashBidiMap_
<br>
Vi har valgt å ha et eget map-interface som vi kaller Dictionary, det er her alle oversettelsene ligger lagret.
Vi har brukt et library fra apache commons collections, for å bruke mapet dualHashBidiMap, som implementerer en metode for å
hente ut key ved hjelp av value, så vel som value ved hjelp av key.
<br>
Kildekode: https://commons.apache.org/proper/commons-collections/apidocs/org/apache/commons/collections4/bidimap/DualHashBidiMap.html

3: _Mocking_
<br>
"Fake it 'till you make it" - stubs
Vi har implementert innhenting av data som er mocket fra Mockito.
<br>
Kilde: https://site.mockito.org

4: _Code coverage_
<br>
Vi har brukt code coverage-verktøyet som er innebygd i IntelliJ.