import org.junit.*;
import java.io.IOException;
import java.util.LinkedList;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class BrailleTranslatorTest {
    BrailleTranslator translator;
    Parser parser;

    @Before
    public void setUp() throws Exception {
        translator = new BrailleTranslator();
        parser = mock(Parser.class);
        when(parser.readFile("alfa.txt")).thenReturn("Text to translate to braille! -what?- Nice, dude.");
        when(parser.readFile("braille.txt")).thenReturn("⠠⠞⠑⠭⠞ ⠖ ⠞⠗⠁⠝⠎⠇⠁⠞⠑ ⠖ ⠃⠗⠁⠊⠇⠇⠑⠖ ⠤⠺⠓⠁⠞⠦⠤ ⠠⠝⠊⠉⠑⠂ ⠙⠥⠙⠑⠲");
        when(parser.writeFile(anyString(), anyString())).thenReturn(true);
    }

    // Tools
    @Test
    public void dictionary_getKey() {
        assertEquals((Character) 'a', translator.letterCharacterMap.getKey('⠁'));
    }

    @Test
    public void dictionary_getValue() {
        assertEquals((Character) '⠁', translator.letterCharacterMap.get('a'));
    }

    @Test
    public void stringToList() {
        LinkedList<String> in = new LinkedList<>() {{
            add("Hello");
            add("you");
            add("are");
            add("nice!");
        }};
        assertEquals(in, translator.stringToList("Hello\n you are nice!"));
    }

    @Test
    public void listToString() {
        LinkedList<String> in = new LinkedList<>() {{
            add("Hello");
            add("you");
            add("are");
            add("nice.");
        }};
        assertEquals("Hello you are nice.", translator.listToString(in));
    }

    @Test
    public void integrationTest_sameResult() {
        String initialString = "Hello I am very happy to be able to test this string. In fact this is a great way to test many thing, for instance: The ability to translate";
        parser = new Parser();
        parser.writeFile("test.txt", initialString);

        translator.setMode(Translator.ALFA_TO_BRAILLE_MODE);
        translator.translateFile("test.txt", "testOut.txt", parser);

        translator.setMode(Translator.BRAILLE_TO_ALFA_MODE);
        translator.translateFile("testOut.txt", "finalTest.txt", parser);

        try {
            assertEquals(parser.readFile("finalTest.txt"), initialString);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void translate_staticVersion_sameResult() {
        String text = "This is the 1 test string, 123!";
        translator = new BrailleTranslator(text, Translator.ALFA_TO_BRAILLE_MODE);
        translator.translate();
        assertEquals(translator.getText(), BrailleTranslator.translate(text, Translator.ALFA_TO_BRAILLE_MODE));
    }

    @Test
    public void translateFile_AlfaToBraille() {
        translator.setMode(Translator.ALFA_TO_BRAILLE_MODE);
        translator.translateFile("alfa.txt", "braille.txt", parser);
        assertEquals("⠠⠞⠑⠭⠞ ⠖ ⠞⠗⠁⠝⠎⠇⠁⠞⠑ ⠖ ⠃⠗⠁⠊⠇⠇⠑⠖ ⠤⠺⠓⠁⠞⠦⠤ ⠠⠝⠊⠉⠑⠂ ⠙⠥⠙⠑⠲", translator.getText());
    }

    @Test
    public void translateFile_BrailleToAlfa() {
        translator.setMode(Translator.BRAILLE_TO_ALFA_MODE);
        translator.translateFile("braille.txt", "alfa.txt", parser);
        assertEquals("Text to translate to braille! -what?- Nice, dude.", translator.getText());
    }

    @Test
    public void alfaToBraille_numberExtractor() {
        translator = new BrailleTranslator("23 04 96 12345 6789 0", Translator.ALFA_TO_BRAILLE_MODE);
        translator.numberExtractor();
        assertEquals("⠼⠃⠉ ⠼⠚⠙ ⠼⠊⠋ ⠼⠁⠃⠉⠙⠑ ⠼⠋⠛⠓⠊ ⠼⠚", translator.getText());
    }

    @Test
    public void alfaToBraille_capsHandler() {
        translator = new BrailleTranslator("HEI dU Er pEn", Translator.ALFA_TO_BRAILLE_MODE);
        translator.capsHandler();
        assertEquals("⠠h⠠e⠠i d⠠u ⠠er p⠠en", translator.getText());
    }

    @Test
    public void alfaToBraille_buildTranslation() {
        translator = new BrailleTranslator("⠠hello, ⠠i am ⠠groot! ⠠the big tree?", Translator.ALFA_TO_BRAILLE_MODE);
        translator.buildTranslation();
        assertEquals("⠠⠓⠑⠇⠇⠕⠂ ⠠⠊ ⠁⠍ ⠠⠛⠗⠕⠕⠞⠖ ⠠⠮ ⠃⠊⠛ ⠞⠗⠑⠑⠦", translator.getText());
    }

    @Test
    public void alfaToBraille_runThrough() {
        translator = new BrailleTranslator("123 The big, number.", Translator.ALFA_TO_BRAILLE_MODE);
        translator.translate();
        assertEquals("⠼⠁⠃⠉ ⠠⠮ ⠃⠊⠛⠂ ⠝⠥⠍⠃⠑⠗⠲", translator.getText());
    }

    @Test
    public void brailleToAlfa_numberExtractor() {
        translator = new BrailleTranslator("⠼⠃⠉ ⠼⠚⠙ ⠼⠊⠋ ⠼⠁⠃⠉⠙⠑ ⠼⠋⠛⠓⠊ ⠼⠚", Translator.BRAILLE_TO_ALFA_MODE);
        translator.numberExtractor();
        assertEquals("23 04 96 12345 6789 0", translator.getText());
    }

    @Test
    public void brailleToAlfaContractions() {
        translator = new BrailleTranslator("⠠⠮ ⠠⠮", Translator.BRAILLE_TO_ALFA_MODE);
        translator.brailleToAlfaContractions();
        assertEquals("⠠the ⠠the", translator.getText());
    }

    @Test
    public void brailleToAlfa_buildTranslation() {
        translator = new BrailleTranslator("⠠the ⠞⠗⠑⠑⠖", Translator.BRAILLE_TO_ALFA_MODE);
        translator.brailleToAlfaBuildTranslation();
        assertEquals("⠠the tree!", translator.getText());
    }

    @Test
    public void brailleToAlfa_capsHandler() {
        translator = new BrailleTranslator("⠠the tree!", Translator.BRAILLE_TO_ALFA_MODE);
        translator.capsHandler();
        assertEquals("The tree!", translator.getText());
    }

    @Test
    public void brailleToAlfa_runThrough() {
        translator = new BrailleTranslator("⠼⠁⠃⠉ ⠠⠮ ⠃⠊⠛⠂ ⠝⠥⠍⠃⠑⠗⠲", Translator.BRAILLE_TO_ALFA_MODE);
        translator.translate();
        assertEquals("123 The big, number.", translator.getText());
    }
}