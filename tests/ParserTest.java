import org.junit.Before;
import org.junit.Test;
import java.io.File;
import java.io.IOException;
import static org.junit.Assert.*;

public class ParserTest {
    Parser parser;
    String path = new File("./src/testParser.txt").getPath();

    @Before
    public void setParser() throws Exception {
        parser = new Parser();
    }

    @Test
    public void readFile() {
        try {
            assertEquals("Yo, du er kjempekul, det synes jeg altså", parser.readFile(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void readFile_IOException() {
        String errr = "0";
        try {
            errr = parser.readFile("dfgdgfdgdgdg");
        } catch (IOException e) {
            errr = "1";
        }
        assertEquals("1", errr);
    }

    @Test
    public void writeFile() {
        assertEquals(true, parser.writeFile(path, "Yo, du er \nkjempekul, \n det synes jeg \n \n    \n         altså"));
    }
}